#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "format.h"
#include "loader.h"

#define L (19) /* the size of an array will be 2^L */
#define S (1U << L) /* size of hash table */
/* we'll keep requirements together with symbols in hash table;
 * this address as not used by us as symbols' address will let us distinguish
 * requirements from symbols */
#define REQ_ADDR 0
#define L_SYMS 0 /* looking for symbols */
#define L_REQS 1 /* looking for requirements' names */

/* if the cond is true, it saves the errno value and goes to appropriate
 * label to release resources */
#define TRY(cond, ev, label) if (cond) { ev = errno; goto label; }
/* similar as above but the the error value equals err */
#define TRY_SET(cond, ev, err, label) if (cond) { ev = err; goto label; }

/* making kernel style list with basic functionality */
#define list_entry(ptr, type, member) \
    ((type*)((char *)(ptr) - (unsigned long)(&((type *)0)->member)))

#define list_for_each(pos, head) \
    for (pos = (head)->next; pos != (head); pos = pos->next)

#define list_for_each_safe(pos, n, head) \
    for (pos = (head)->next, n = pos->next; pos != (head); \
            pos = n, n = pos->next)

#define list_init(head) (head)->next = (head)->prev = (head)

typedef struct List {
    struct List *prev, *next;
} List;

/* struct for both symbols and loaded libraries */
typedef struct {
    char *name;
    Elf32_Addr addr;
    List list;
} Entry;

/* for keeping information about added entries */
typedef struct Occur {
    Entry *e;
    List list;
} Occur;

/* for keeping information about made mappings */
typedef struct Mapping {
    Elf32_Word mem_size;
    Elf32_Addr v_addr;
    List list;
} Mapping;

/* mask needed for hash table; bitwise AND will be applied instead of module
 * operation */
static unsigned int mask = ~((~0U) << L);
static List htab[S]; /* hash table */
static int64_t linking_id; /* linking id of the libraries */
/* indicating whether there was correctly read library */
static int already_started = 0;

/* adding elt after the place */
static void list_add(List *place, List *elt) {
    elt->next = place->next;
    place->next->prev = elt;
    elt->prev = place;
    place->next = elt;
}

__attribute__((constructor)) static void init_htable() {
    int i;
    for (i = 0; i < S; i++)
        list_init(&htab[i]);
}

__attribute__((destructor)) static void clear_htable() {
    Entry *e;
    List *pos, *n;
    int i;
    for (i = 0; i < S; i++)
        list_for_each_safe(pos, n, &htab[i]) {
            e = list_entry(pos, Entry, list);
            free(e->name);
            free(e);
        }
}

/* using djb2 hashing */
static unsigned int get_hash(const char *str) {
    unsigned int hash = 5381;
    int c;
    while ((c = *(str++)))
        hash = ((hash << 5) + hash) + (unsigned char)c;
    return hash;
}

static Entry* add_to_hashtable(const char *name, Elf32_Addr addr) {
    char *ptr;
    Entry *e;
    int ev;
    List *head = &htab[get_hash(name) & mask];
    if ((e = malloc(sizeof(Entry))) == NULL)
        return NULL;
    if ((ptr = malloc(strlen(name) + 1)) == NULL) {
        ev = errno;
        free(e);
        errno = ev;
        return NULL;
    }
    strcpy(ptr, name);
    e->name = ptr;
    e->addr = addr;
    list_add(head, &e->list);
    return e;
}

/* reqs set to L_REQS means we're looking for requirements, L_SYMS - looking
 * for symbols */
static Entry* find_in_hashtable(const char *name, int reqs) {
    List *head = &htab[get_hash(name) & mask], *pos;
    Entry *e;
    list_for_each(pos, head) {
        e = list_entry(pos, Entry, list);
        if (strcmp(e->name, name) == 0 && (reqs == L_SYMS ||
                e->addr == REQ_ADDR))
            return e;
    }
    return NULL;
}

/* removing a single entry from hash table; we assume that it's not the list
 * head */
static void remove_from_hashtable(Entry *e) {
    List *pr = e->list.prev, *n = e->list.next;
    pr->next = n;
    n->prev = pr;
    free(e->name);
    free(e);
}

/* comparing shifted first with the second */
static int shifted_eq(const char *first, const char *second) {
    int res = 1, i;
    if (strlen(first) != strlen(second))
        res = 0;
    for (i = 0; i < strlen(first); i++)
        if ((*(first + i) + CSO_SHIFT) != *(second + i))
            res = 0;
    return res;
}

/* reading a cso table and returning the pointer */
static void* read_tab(int fd, const Cso_hdr *hdr, int cso_ind) {
    void *ptr;
    int ev = 0;
    if ((ptr = malloc(hdr->tables[cso_ind].size)) == NULL)
        return NULL;
    if (pread(fd, ptr, hdr->tables[cso_ind].size,
              hdr->tables[cso_ind].offset) == -1) {
        ev = errno;
        free(ptr);
        errno = ev;
        return NULL;
    }
    return ptr;
}

/* getting mmap args from cso table index */
static void get_mmap_args(int fd, int ind, int *flags,
                          int *prot, int *fdarg) {
    *prot = PROT_READ, *flags = 0;
    if (ind & P_NULL) {
        *flags |= MAP_ANONYMOUS;
        *fdarg = -1;
    }
    else
        *fdarg = fd;
    if (ind & P_EXEC)
        *prot |= PROT_EXEC;
    if (ind & P_WRITE)
        *prot |= PROT_WRITE;
    *flags |= MAP_FIXED;
    *flags |= MAP_PRIVATE;
}

/* adding to hashtable with adding information to occurs, if succeed */
static int safe_add(const char *name, Elf32_Addr addr, Occur *occurs) {
    Occur *occ;
    int err;
    if ((occ = malloc(sizeof(Occur))) == NULL)
        return -1;
    if ((occ->e = add_to_hashtable(name, addr)) == NULL) {
        err = errno;
        free(occ);
        errno = err;
        return -1;
    }
    list_add(&occurs->list, &occ->list);
    return 0;
}

/* mapping with adding information to mapps, if succeed */
static int safe_map(Cso_phdr *cur, int ind, int fd, Mapping *mapps) {
    int prot, flags, err, fdarg;
    Mapping *m;
    get_mmap_args(fd, ind, &flags, &prot, &fdarg);
    if ((m = malloc(sizeof(Mapping))) == NULL)
        return -1;
    if (mmap((void*)cur->v_addr, cur->mem_size, prot, flags, fdarg,
             cur->offset) == MAP_FAILED) {
        err = errno;
        free(m);
        errno = err;
        return -1;
    }
    m->mem_size = cur->mem_size;
    m->v_addr = cur->v_addr;
    list_add(&mapps->list, &m->list);
    return 0;
}

/* we will store errno from system functions in a variable ev, because we'll
 * make later other calls (e.g. free, close) */
static int rec_load(const char *name, Occur *occurs, Mapping *mapps) {
    int fd, i, ev = 0, syms_count, reqs_count, ok = 0, changed_id = 0;
    char *str_tab;
    Cso_hdr hdr;
    Cso_sym *sym_tab;
    Cso_req *req_tab;

    if ((fd = open(name, O_RDONLY)) == -1)
        return -1;
    TRY(read(fd, &hdr, sizeof(hdr)) == -1, ev, close_fd);
    TRY_SET(strcmp(hdr.ident, CSO_IDENT) != 0, ev, EINVAL, close_fd);
    if (already_started)
        TRY_SET(linking_id != hdr.linking_id, ev, EINVAL, close_fd);

    TRY((str_tab = read_tab(fd, &hdr, STR_POS)) == NULL, ev, close_fd);
    TRY_SET(!shifted_eq(name, str_tab + hdr.file_name), ev, EINVAL, free_str);
    if (find_in_hashtable(name, L_REQS)) { /* library already loaded */
        ok = 1;
        goto free_str;
    }
    TRY(safe_add(name, REQ_ADDR, occurs) == -1, ev, free_str);
    TRY((sym_tab = read_tab(fd, &hdr, SYM_POS)) == NULL, ev, free_str);
    TRY((req_tab = read_tab(fd, &hdr, REQ_POS)) == NULL, ev, free_sym);

    syms_count = hdr.tables[SYM_POS].size / sizeof(Cso_sym);
    for (i = 0; i < syms_count; i++)
        TRY(safe_add(str_tab + sym_tab[i].name,
                     sym_tab[i].addr, occurs) == -1, ev, free_req);

    for (i = 0; i < SEGM_SIZE; i++) {
        Cso_phdr *cur = &hdr.seg_heads[i];
        if (cur->mem_size)/* if segment is not empty */
            TRY(safe_map(cur, i, fd, mapps) == -1, ev, free_req);
    }

    if (!already_started) {
        already_started = 1;
        changed_id = 1;
        linking_id = hdr.linking_id;
    }

    reqs_count = hdr.tables[REQ_POS].size / sizeof(Cso_req);
    for (i = 0; i < reqs_count; i++)
        if (!find_in_hashtable(str_tab + req_tab[i], L_REQS))
            TRY(rec_load(str_tab + req_tab[i], occurs, mapps), ev, free_req);
    ok = 1;
free_req:
    free(req_tab);
free_sym:
    free(sym_tab);
free_str:
    free(str_tab);
close_fd:
    if (close(fd) == -1) /* close can sometimes tell about earlier errors */
        return -1;
    if (!ok) {
        if (changed_id)
            already_started = 0;
        errno = ev;
        return -1;
    }
    return 0;
}

/* we'll store all made changes into lists to make it possible to quickly
 * roll it back in case any errors occur */
int library_load(const char *name) {
    Occur occurs, *occ;
    Mapping mapps, *m;
    int err, ok;
    List *pos, *n;
    list_init(&(occurs.list));
    list_init(&(mapps.list));
    if (rec_load(name, &occurs, &mapps) == -1) {
        ok = 0;
        err = errno;
    }
    else
        ok = 1;
    list_for_each_safe(pos, n, &occurs.list) {
        occ = list_entry(pos, Occur, list);
        if (!ok)
            remove_from_hashtable(occ->e);
        free(occ);
    }
    list_for_each_safe(pos, n, &mapps.list) {
        m = list_entry(pos, Mapping, list);
        /* we're ignoring the errors since there's nothing we can do about it
         * anyway */
        if (!ok)
            munmap((void*)m->v_addr, m->mem_size);
        free(m);
    }
    if (!ok) {
        errno = err;
        return -1;
    }
    return 0;
}

void *library_getsym(const char *name) {
    Entry *e = find_in_hashtable(name, L_SYMS);
    if (e)
        return (void*)e->addr;
    return NULL;
}
