#include <stdarg.h>
int (*pvprintf)(const char *fmt, va_list va) = 0;
int printf(const char *fmt, ...) {
	va_list va;
	va_start(va, fmt);
	pvprintf(fmt, va);
	va_end(va);
}
