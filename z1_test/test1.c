#include "loader.h"
#include <stdio.h>

int main() {
    if (library_load("printf.cso"))
        return 1;
    int (**pvprintf)(const char *fmt, va_list va);
    pvprintf = library_getsym("pvprintf");
    *pvprintf = vprintf;
    void (*hello)();
    hello = library_getsym("hello");
    if (hello)
	    return 1;
    if (library_load("hello.cso"))
        return 1;
    hello = library_getsym("hello");
    hello();
    return 0;
}
