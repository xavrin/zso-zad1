#!/bin/sh
gcc -m32 -Wl,-rpath=. test1.c libloader.so -g -o test1
gcc -m32 -Wl,-rpath=. test2.c libloader.so -g -o test2
gcc -m32 -Wl,-rpath=. test3.c libloader.so -g -o test3
gcc -m32 -ffreestanding -c t1.c t2.c t3.c t4.c t5.c hello.c printf.c
./linker t1.o t2.o t3.o t4.o t5.o
./linker hello.o printf.o
if ./test1 > test.out
then
        if cmp test.out test1.out
        then
                echo "test1 PASSED [3p]"
        else
                echo "test1 FAILED [wrong output]"
        fi
else
        echo "test1 FAILED [error code $?]"
fi
if ./test2 > test.out
then
        if cmp test.out test2.out
        then
                echo "test2 PASSED [3p]"
        else
                echo "test2 FAILED [wrong output]"
        fi
else
        echo "test2 FAILED [error code $?]"
fi
rm t1.cso t2.cso
if ./test3 > test.out
then
        if cmp test.out test3.out
        then
                echo "test3 PASSED [4p]"
        else
                echo "test3 FAILED [wrong output]"
        fi
else
        echo "test3 FAILED [error code $?]"
fi
