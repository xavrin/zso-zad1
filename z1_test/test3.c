#include "loader.h"
#include <stdio.h>

int main() {
    if (library_load("t4.cso"))
        return 1;
    if (library_load("t5.cso"))
        return 1;
    int *x = library_getsym("x");
    int (*getx)(int) = library_getsym("getx");
    void (*setx)(int, int) = library_getsym("setx");
    printf("%d %d\n", getx(0), getx(1));
    x[0] = 4;
    setx(1, 7);
    printf("%d %d\n", getx(0), x[1]);
    return 0;
}
