#include "loader.h"
#include <stdio.h>

int main() {
    if (library_load("t2.cso"))
        return 1;
    int *x = library_getsym("x");
    printf("%d %d %d\n", x[0], x[1], x[2]);
    int (*g)();
    g = library_getsym("g");
    printf("%d\n", g());
    return 0;
}
