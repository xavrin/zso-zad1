#include "linker.h"

using std::string;
using std::map;
using std::set;
using std::vector;
using std::pair;
using std::ifstream;
using std::ofstream;
using std::shared_ptr;
using std::cerr;
using std::endl;
using std::unique_ptr;

const Elf32_Addr LOWER_BOUND = 0x10000000, UPPER_BOUND = 0x6fffffff;
static const int PAGE_SIZE = sysconf(_SC_PAGE_SIZE); // page size in bytes

int64_t get_time_stamp() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count();
}

// id of the linking; all files from the same consolidation will have
// the same value
int64_t get_linking_id() {
    static const int64_t MAGIC_COOF = 345093405812033545LL,
                         MAGIC_ADD = 304958304958304934LL;
    static int64_t linking_id = MAGIC_COOF * get_time_stamp() + MAGIC_ADD;
    return linking_id;
}

// reserves in virtual memory of the libraries address for holding
// 'size' bytes
Elf32_Addr reserve_addr(Elf32_Word size) {
    static Elf32_Addr first_free = LOWER_BOUND;
    /* we want to start addresses on the beginning of a page */
    first_free += (PAGE_SIZE - (first_free % PAGE_SIZE)) % PAGE_SIZE;
    if (first_free + size > UPPER_BOUND)
        throw LinkingError("Too much memory required");
    Elf32_Addr tmp = first_free;
    first_free += size;
    return tmp;
}

// changes format of 'name' to .cso; if it has not an extensions, the
// functions adds one
string get_cso_name(const string &name) {
    size_t dot = name.find_last_of('.');
    if (dot == string::npos)
        return name + ".cso";
    else
        return name.substr(0, dot) + ".cso";
}

// shifting string's characters
string get_shifted_string(const string &name) {
    string res = "";
    for (char c : name)
        res += (c + CSO_SHIFT);
    return res;
}

/* getting index into Cso segment table */
int get_flags(Elf32_Shdr &sh) {
    int res = 0;
    if (sh.sh_flags & SHF_WRITE)
        res |= P_WRITE;
    if (sh.sh_flags & SHF_EXECINSTR)
        res |= P_EXEC;
    if (sh.sh_type == SHT_NOBITS)
        res |= P_NULL;
    return res;
}

// writing trash into file to make offset congruent to page size
void adjust_to_ps(ofstream &file) {
    char zero = 0;
    Elf32_Off position = file.tellp();
    int diff = (PAGE_SIZE - (position % PAGE_SIZE)) % PAGE_SIZE;
    for (int i = 0; i < diff; i++)
        file.write(&zero, 1);
}

bool ElfFile::is_format_correct(const Elf32_Ehdr &h) {
    return strncmp((const char*)h.e_ident, (const char*)ELFMAG, SELFMAG) == 0
           && h.e_ident[EI_CLASS] == ELFCLASS32
           && h.e_ident[EI_DATA] == ELFDATA2LSB
           && h.e_ident[EI_VERSION] == EV_CURRENT
           && h.e_version == EV_CURRENT
           && h.e_machine == EM_386
           && h.e_type == ET_REL;
}

bool ElfFile::is_correct_section(Elf32_Section s) {
    return s < header.e_shnum;
}

char* ElfFile::read_section(Elf32_Shdr &sh) {
    char *ptr;
    is.seekg(sh.sh_offset);
    ptr = new char [sh.sh_size];
    try {
        is.read(ptr, sh.sh_size);
    }
    catch (...) {
        delete [] ptr;
        throw;
    }
    return ptr;
}

void ElfFile::loop_through_sections() {
    sym_ind = -1;
    for (Elf32_Section i = 0; i < header.e_shnum; i++) {
        if (shead_tab[i].sh_type == SHT_SYMTAB) {
            if (sym_ind != -1)
                throw LinkingError("File contains multiple symbol sections");
            sym_ind = i;
        }
        if ((shead_tab[i].sh_flags & SHF_ALLOC) && shead_tab[i].sh_size)
            groups[get_flags(shead_tab[i])].sections.push_back(i);
    }
    if (sym_ind == -1)
        throw LinkingError("File contains no symbol section");
}

ElfFile::ElfFile(string path)
    : is(path, std::ios::in | std::ios::binary), file_name(path),
      are_addresses_fixed(false)
{
    // making file operations throw exceptions
    is.exceptions(std::ifstream::failbit | std::ifstream::badbit |
                  std::ifstream::eofbit);
    is.read((char*)&header, sizeof(header));
    if (!is_format_correct(header))
        throw LinkingError("Not an ELF 32 bit file");
    is.seekg(header.e_shoff);
    shead_tab = std::move(unique_ptr<Elf32_Shdr[]>(
        new Elf32_Shdr [header.e_shnum]));
    is.read((char*)shead_tab.get(), header.e_shnum * sizeof(Elf32_Shdr));
    loop_through_sections();
}

void ElfFile::distribute_addresses() {
    assert(!are_addresses_fixed);
    for (int i = 0; i < SEGM_SIZE; i++)
        if (!groups[i].sections.empty()) {
            Elf32_Addr offset(0);
            Group &gr = groups[i];
            gr.mem_size = 0;
            for (auto j : gr.sections) // counting size of the group
                gr.mem_size += shead_tab[j].sh_size;
            gr.v_addr = reserve_addr(gr.mem_size);
            // giving allocatable sections their fixed virtual addresses
            for (auto j : gr.sections) {
                shead_tab[j].sh_addr = gr.v_addr + offset;
                offset += shead_tab[j].sh_size;
            }
        }
    are_addresses_fixed = true;
}

void ElfFile::setup_symbols(set<string> &undef, map<string, Symb> &def) {
    distribute_addresses();
    auto &sym_sec = shead_tab[sym_ind];
    int sym_count = sym_sec.sh_size / sym_sec.sh_entsize;
    unique_ptr<Elf32_Sym[]> sym_tab((Elf32_Sym*)read_section(sym_sec));
    unique_ptr<char[]> str_tab(read_section(shead_tab[sym_sec.sh_link]));

    for (int i = 1; i < sym_count; i++) { // ignoring the first special symbol
        auto &s = sym_tab[i];
        string name = str_tab.get() + s.st_name;
        switch (ELF32_ST_TYPE(s.st_info)) {
            case STT_NOTYPE: {
                if (def.count(name) == 0)
                    undef.insert(name);
                break;
            }
            case STT_FUNC:
            case STT_OBJECT: {
                // if something is not STB_LOCAL, we treat it as STB_GLOBAL
                if (!(ELF32_ST_BIND(s.st_info) == STB_LOCAL) &&
                        is_correct_section(s.st_shndx)) {
                    if (undef.count(name))
                        undef.erase(name);
                    if (def.count(name))
                        throw LinkingError("Multiple symbols with the "
                            "same name");
                    Elf32_Addr addr = shead_tab[s.st_shndx].sh_addr +
                        s.st_value;
                    exported.push_back(make_pair(name, addr));
                    if (!shead_tab[s.st_shndx].sh_flags & SHF_ALLOC)
                        throw LinkingError("Symbol referencing "
                            "non-allocatable section");
                    def[name] = Symb(addr, file_name);
                }
                break;
            }
            default:
                break; // we're ignoring other types
        }
    }
}

ElfFile::~ElfFile() {
    is.close();
}

vector<shared_ptr<char> > ElfFile::setup_allocatable() {
    vector<shared_ptr<char> > sections;
    for (Elf32_Section i = 0; i < header.e_shnum; i++) {
        if ((shead_tab[i].sh_flags & SHF_ALLOC) && shead_tab[i].sh_size > 0)
            sections.push_back(shared_ptr<char>(read_section(shead_tab[i]),
                               [](char *p) { delete [] p; }));
        else
            sections.push_back(nullptr);
    }
    return sections;
}

Elf32_Addr ElfFile::resolve_symbol(const Elf32_Sym &s,
                                   const map<string, Symb> &def,
                                   const unique_ptr<char[]> &str_tab) {
    Elf32_Addr addr;
    string name = str_tab.get() + s.st_name;
    switch (ELF32_ST_TYPE(s.st_info)) {
        case STT_NOTYPE:
            requirements.insert(get_cso_name(def.at(name).filename));
            addr = def.at(name).addr; break;
        case STT_OBJECT:
        case STT_FUNC:
            addr = shead_tab[s.st_shndx].sh_addr + s.st_value;
            break;
        case STT_SECTION:
            if (!shead_tab[s.st_shndx].sh_flags & SHF_ALLOC)
                throw LinkingError("Symbol in non-allocatable section");
            addr = shead_tab[s.st_shndx].sh_addr;
            break;
        default:
            throw LinkingError("Unknown kind of symbol");
    }
    return addr;
}

Elf32_Addr ElfFile::get_relocation_value(const Elf32_Rel &r,
                                         const Elf32_Addr &addr,
                                         const Elf32_Addr &rel_addr,
                                         const Elf32_Addr &addend) {
    Elf32_Addr result;
    switch (ELF32_R_TYPE(r.r_info)) {
        case R_386_32:
            result = addr + addend; break;
        case R_386_PC32:
            result = addr - rel_addr + addend; break;
        default:
            throw LinkingError("Unknown kind of relocation");
    }
    return result;
}

void ElfFile::make_relocations(const vector<shared_ptr<char> > &sections,
                               const map<string, Symb> &def) {
    auto &sym_sec = shead_tab[sym_ind];
    unique_ptr<Elf32_Sym[]> sym_tab((Elf32_Sym*)read_section(sym_sec));
    unique_ptr<char[]> str_tab(read_section(shead_tab[sym_sec.sh_link]));
    for (Elf32_Section i = 1; i < header.e_shnum; i++)
        if (shead_tab[i].sh_type == SHT_REL) {
            unique_ptr<Elf32_Rel[]> rels(
                (Elf32_Rel*)read_section(shead_tab[i]));
            int amount = shead_tab[i].sh_size / shead_tab[i].sh_entsize;
            for (int j = 0; j < amount; j++) {
                Elf32_Rel &r = rels[j];
                // the index of relocated section
                Elf32_Section rel_ind = shead_tab[i].sh_info;
                if (!is_correct_section(rel_ind))
                    throw LinkingError("Relocation in a special section");
                // relocated symbol
                Elf32_Sym s = sym_tab[ELF32_R_SYM(r.r_info)];
                // virtual address of the relocated field
                Elf32_Addr rel_addr = shead_tab[rel_ind].sh_addr + r.r_offset,
                           addr = resolve_symbol(s, def, str_tab);
                // the current memory address of the section being relocated
                shared_ptr<char> sec = sections[rel_ind];
                if (!sec)
                    throw LinkingError("Relocation in non-allocatable "
                                       "section");
                // pointer to the relocated place in memory NOW
                Elf32_Addr *rel = (Elf32_Addr*)(sec.get() + r.r_offset);
                // assigning the required relocation
                *rel = get_relocation_value(r, addr, rel_addr, *rel);
            }
        }
}

Elf32_Half ElfFile::get_str_size() {
    Elf32_Half str_size = 0;
    for (auto &i : requirements)
        str_size += i.size() + 1;
    for (auto &i : exported)
        str_size += i.first.size() + 1;
    return get_cso_name(file_name).size() + 1 + str_size;
}

void ElfFile::create_cso_file(vector<shared_ptr<char> > &sections) {
    Cso_hdr hdr = {};
    vector<shared_ptr<char> > tables;
    hdr.tables[SYM_POS].size = exported.size() * sizeof(Cso_sym);
    hdr.tables[REQ_POS].size = requirements.size() * sizeof(Cso_req);
    hdr.tables[STR_POS].size = get_str_size();
    for (int i = 0; i < TAB_NUM; i++)
        tables.push_back(shared_ptr<char>(new char [hdr.tables[i].size],
            [](char *p) { delete [] p; }));
    // pointer to current position in string table and one to start position
    char *ptr, *base_ptr;
    ptr = base_ptr = tables[STR_POS].get();
    int ind = 0;
    Cso_req *reqs = (Cso_req*)tables[REQ_POS].get();
    for (auto &req : requirements) {
        strcpy(ptr, req.c_str());
        reqs[ind] = ptr - base_ptr;
        ptr += req.size() + 1;
        ind++;
    }
    Cso_sym *syms = (Cso_sym*)tables[SYM_POS].get();
    for (size_t i = 0; i < exported.size(); i++) {
        strcpy(ptr, exported[i].first.c_str());
        syms[i].name = ptr - base_ptr;
        syms[i].addr = exported[i].second;
        ptr += exported[i].first.size() + 1;
    }
    string cso_name = get_cso_name(file_name);
    strcpy(ptr, get_shifted_string(cso_name).c_str());
    hdr.file_name = ptr - base_ptr;
    strcpy(hdr.ident, CSO_IDENT);
    hdr.linking_id = get_linking_id();
    ofstream cso_file(cso_name, std::ios::out | std::ios::binary);
    cso_file.write((char*)&hdr, sizeof(hdr));
    for (int i = 0; i < TAB_NUM; i++) {
        adjust_to_ps(cso_file);
        hdr.tables[i].offset = cso_file.tellp();
        cso_file.write(tables[i].get(), hdr.tables[i].size);
    }
    for (int i = 0; i < SEGM_SIZE; i++) {
        if (!groups[i].sections.empty()) {
            Group &gr = groups[i];
            adjust_to_ps(cso_file);
            auto &segh = hdr.seg_heads[i];
            segh.offset = cso_file.tellp();
            segh.mem_size = gr.mem_size;
            segh.v_addr = gr.v_addr;
            for (auto &j : gr.sections)
                cso_file.write(sections[j].get(), shead_tab[j].sh_size);
        }
        else
            hdr.seg_heads[i].mem_size = 0;
    }
    cso_file.seekp(0);
    cso_file.write((char*)&hdr, sizeof(hdr));
    cso_file.close();
}

void ElfFile::relocate_and_create_cso(map<string, Symb> &def) {
    if (!are_addresses_fixed)
        throw LinkingError("Adresses are not fixed yet");
    vector<shared_ptr<char> > sections = setup_allocatable();
    make_relocations(sections, def);
    create_cso_file(sections);
}

int main(int argc, char* argv[]) {
    set<string> undef;
    map<string, Symb> def;
    int ind; // index of currently processed file
    if (argc <= 1) {
        cerr << "Error: no input files; terminating" << endl;
        return EXIT_FAILURE;
    }
    try {
        vector<shared_ptr<ElfFile> > files;
        for (ind = 1; ind < argc; ind++) {
            files.push_back(shared_ptr<ElfFile>(new ElfFile(argv[ind])));
            files[ind - 1]->setup_symbols(undef, def);
        }

        if (!undef.empty()) {
            const char* first = undef.begin()->c_str();
            cerr << "Error: some symbols are missing: " << first << endl;
            return EXIT_FAILURE;
        }
        for (ind = 1; ind < argc; ind++)
            files[ind-1]->relocate_and_create_cso(def);
    }
    catch (LinkingError &le) {
        cerr << "Error in file " << argv[ind] << ": " << le.what() << endl;
        return EXIT_FAILURE;
    }
    catch (std::ios_base::failure &f) {
        cerr << "Error in file " << argv[ind] << ": "
             << "the file cannot be correctly read" << endl;
        return EXIT_FAILURE;
    }
    catch (std::exception &e) {
        cerr << "Error: " << e.what() << endl;
        return EXIT_FAILURE;
    }
    return 0;
}
