#include <assert.h>
#include "loader.h"

int main() {
    assert(library_load("e.cso") == 0);
    assert(*(int*)library_getsym("x") == 4);
    assert(library_load("f.cso") == -1);
    assert(*(int*)library_getsym("x") == 4);
    return 0;
}
