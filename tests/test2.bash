#!/bin/bash
for i in e f g
do
    gcc -m32 -c $i.c
done
gcc -m32 -Wl,-rpath=. test_cycle.c libloader.so -g -o test_cycle
gcc -m32 -Wl,-rpath=. test_remove.c libloader.so -g -o test_remove
gcc -m32 -Wl,-rpath=. test_retry.c libloader.so -g -o test_retry
if ./linker f.o e.o g.o && ./test_cycle
then
    echo "OK"
else
    echo "FAIL"
fi
valgrind ./test_retry
rm g.cso
valgrind ./test_remove
rm e.o f.o g.o *.cso test_cycle test_remove test_retry
