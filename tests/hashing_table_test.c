#include <assert.h>
#include "../loader.c"

int main() {
    Entry e;
    Entry *eptr;
    int N = 1, i;
    char* tab1[] = {"A", "BBBB", "CCC", "fasdfasdf", "asdf", "DDDD", "asdfsdf", "fasdfsdfsdf", "sdfsdfjskdfjdk", "dfsdfdsf"};
    char* tab2[] = {"A", "BBBB", "CCC", "fasdfasdf", "asdf", "DDDD", "asdfsdf", "fasdfsdfsdf", "sdfsdfjskdfjdk", "dfsdfdsf"};
    int addr[] = {1, 2, 3, 4, 5, 6};
    for (i = 0; i < N; i++) {
        Entry e, *tmp;
        e.name = tab1[i]; e.addr = addr[i];
        add_to_hashtable(e.name, e.addr);
        tmp = find_in_hashtable(tab2[i], L_SYMS);
        assert(tmp && strcmp(tmp->name, e.name) == 0 && tmp->addr == e.addr);
    }
    add_to_hashtable("dupa", 4);
    assert(eptr = find_in_hashtable("dupa", L_SYMS));
    remove_from_hashtable(eptr);
    assert(!find_in_hashtable("dupa", L_SYMS));
    return 0;
}
