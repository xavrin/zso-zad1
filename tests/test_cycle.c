#include <assert.h>
#include "loader.h"

int main() {
    if (library_load("f.cso") == -1)
        return 1;
    assert(*((int*)library_getsym("y")) == 6);
    assert(*(int*)library_getsym("x") == 4);
    assert(*(int*)library_getsym("z") == 5);
    return 0;
}
