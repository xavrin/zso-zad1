#include <assert.h>
#include <stdio.h>
#include "loader.h"

int main() {
    assert(library_load("e.cso") == 0);
    assert(*(int*)library_getsym("x") == 4);
    rename("g.cso", "temp.cso");
    assert(library_load("f.cso") == -1);
    rename("temp.cso", "g.cso");
    assert(library_load("f.cso") == 0);
    assert(library_load("f.cso") == 0);
    assert(*(int*)library_getsym("x") == 4);
    assert(*(int*)library_getsym("z") == 5);
    return 0;
}
