#ifndef _FORMAT_H
#define _FORMAT_H

/* we'll use some elf basic types */
#include "elf.h"
#include <stdint.h>

#define CSO_IDENT "\125\100CSO_FILE\50\25"
#define IDENT_SIZE (sizeof(CSO_IDENT))

/* we'll shift characters in file name string to make it a little harder
 * to fake */
#define CSO_SHIFT 5

/* the struct below will store information about loadable data;
 * it holds both the size in file and memory because we don't
 * hold uninitialized data values in file;
 * when mem_size equals zero the entry should be ignored */
typedef struct {
    Elf32_Off offset; /* offset since the beginning of file */
    Elf32_Word mem_size; /* size in memory */
    Elf32_Addr v_addr; /* virtual address of segment */
} Cso_phdr;

/* flags for the segments containing information how it should
 * be mapped into memory */
#define SEGM_SIZE (8)
#define P_EXEC (1 << 0) /* segment is executable */
#define P_WRITE (1 << 1) /* segment is writable */
#define P_NULL (1 << 2) /* segment is zero-initialized */

#define TAB_NUM 3
#define SYM_POS 0 /* position of the symbol table */
#define REQ_POS 1 /* position of the requirements table */
#define STR_POS 2 /* position of the string table */

/* structure holding information about position of tables in
 * the file; however, there will be only 3 tables listed above */
typedef struct {
    Elf32_Off offset; /* offset of the table */
    Elf32_Word size; /* size of the table */
} Cso_table;

typedef struct {
    char ident[IDENT_SIZE]; /* identification of format */
    Elf32_Word file_name; /* name of the file (str table index) */
    int64_t linking_id; /* random magic number from linking */

    Cso_table tables[TAB_NUM]; /* headers of the tables */

    Cso_phdr seg_heads[SEGM_SIZE]; /* headers of segments */
} Cso_hdr;

/* requirement will be an index to string table holding name of the
 * required library */
typedef Elf32_Word Cso_req;

/* format of the symbol table entry; it contains information about the
 * symbols exported by the module */
typedef struct {
    Elf32_Word name; /* name of the symbol */
    Elf32_Addr addr; /* address of the symbol */
} Cso_sym;

#endif
