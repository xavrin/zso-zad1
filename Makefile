CPPFLAGS = -std=c++11 -Wall -m32
SHARED = -m32 -Wall -fPIC -shared
all: linker libloader.so

libloader.so: loader.c format.h loader.h
	gcc $(SHARED) loader.c -o libloader.so -Wl,-soname=libloader.so

linker: linker.cpp elf.h format.h linker.h
	g++ $(CPPFLAGS) $< -o $@
