#include <cstring>
#include <unistd.h>
#include <cassert>

#include <string>
#include <map>
#include <set>
#include <memory>
#include <vector>
#include <utility>
#include <exception>
#include <fstream>
#include <iostream>
#include <chrono>

#include "format.h"
#include "elf.h"

// class for global symbol occurrences
struct Symb {
    Symb(const Elf32_Addr &addr, const std::string &filename)
        : addr(addr), filename(filename) {}
    Symb() {};
    Elf32_Addr addr;
    std::string filename;
};

// class for a group of sections with the same flags
struct Group {
    std::vector<Elf32_Section> sections;
    Elf32_Addr v_addr; // address of the beginning of group
    Elf32_Word mem_size; // size in memory
};

class LinkingError : public virtual std::exception {
    private:
        const char *msg;
    public:
        LinkingError(const char *msg) : msg(msg) {}
        const char* what() const noexcept {
            return msg;
        }
};

// Helping class for ELF file manipulation
class ElfFile {
    private:
        std::ifstream is; // input stream for the file
        std::string file_name; // name of the file
        Elf32_Ehdr header; // elf header
        std::unique_ptr<Elf32_Shdr[]> shead_tab; // section headers table
        // groups of indexes of allocatable sections with the same flags and
        // being all either SHT_NOBITS or SHT_PROGBITS; the index is a bitmask
        // details are described in 'format.h'
        Group groups[SEGM_SIZE];
        // whether the sections from this elf file have already addresses
        bool are_addresses_fixed;
        // symbols exported by the file
        std::vector<std::pair<std::string, Elf32_Addr> > exported;
        // libraries required by the library from the given file
        std::set<std::string> requirements;
        int sym_ind; // index of the symbol table

        // finding symtab and creating groups of sections
        void loop_through_sections();
        // mapping to memory the given section and returning pointer to it
        char* read_section(Elf32_Shdr &sh);
        // checking file's signature
        static bool is_format_correct(const Elf32_Ehdr &h);
        // checking if section with the given index is correct (we don't call
        // SHN_UNDEF incorrect)
        bool is_correct_section(Elf32_Section s);
        // giving fixed addresses to allocatable sections within the file
        void distribute_addresses();
        // mapping to memory allocatable sections
        std::vector<std::shared_ptr<char> > setup_allocatable();
        // getting symbol address and updating requirements, if it's external
        Elf32_Addr resolve_symbol(const Elf32_Sym &s,
                                  const std::map<std::string, Symb> &def,
                                  const std::unique_ptr<char[]> &str_tab);
        Elf32_Addr get_relocation_value(const Elf32_Rel &r,
                                        const Elf32_Addr &addr,
                                        const Elf32_Addr &rel_addr,
                                        const Elf32_Addr &addend);
        // making relocations to the file; sections contains memory addresses
        // to appropriate sections
        void make_relocations(
            const std::vector<std::shared_ptr<char> > &sections,
            const std::map<std::string, Symb> &def);
        // getting size in bytes of string table of the corresponding cso file
        Elf32_Half get_str_size();
        // creating corresponding .cso file
        void create_cso_file(std::vector<std::shared_ptr<char> > &sections);
    public:
        // creates an object from a file from 'path'
        ElfFile(std::string path);
        // associating addresses with symbols present in the module and
        // updating information about external symbols
        void setup_symbols(std::set<std::string> &undef,
                           std::map<std::string, Symb> &def);
        // making relocations and creating the corresponding .cso file
        void relocate_and_create_cso(std::map<std::string, Symb> &def);
        ~ElfFile();
};
