#!/bin/bash
make
cp loader.h linker libloader.so z1_test/
echo "Running main test";
cd z1_test
./test.sh
echo "Running valgrind tests"
echo "Valgrind - linker testing"
if valgrind --leak-check=full --error-exitcode=1 ./linker printf.o hello.o
then
    echo "OK"
else
    echo "FAIL"
fi
echo "Valgrind - loader testing"
for i in 1 2 3
do
    valgrind --leak-check=full ./test$i
done

rm loader.h linker libloader.so *.cso *.o test1 test2 test3
cd ..
mv linker libloader.so tests/
cp loader.h tests/
cd tests
echo "Testing hashtable"
gcc hashing_table_test.c -o htest
if ./htest
then
    echo "OK"
else
    echo "FAIL"
fi
rm htest
for t in 0 1 2
do
    echo "Running test nr $t"
    ./test${t}.bash
done
rm linker libloader.so loader.h
